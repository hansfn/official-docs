[[guide_evaluator]]
=== Goals

This guide provides instructions for creating a disposable Drupal demo application that can be used to evaluate Drupal on your local machine. The demo application is not meant to be used in production.

[TIP]
Read the <<guide_getting_started, Getting Started>> guide if you'd prefer to begin building a custom Drupal application that can be used in a production environment.

[TIP]
Visit link:/try-drupal[Try Drupal] if you'd prefer to use a free, hosted solution for your evaluation.

=== Requirements

In order to install and run a Drupal demo application on your local machine, your system include:

* An accessible command line terminal
* https://www.drupal.org/docs/8/system-requirements/php-requirements[PHP 5.5.9+]

Note that MacOS has PHP installed by default.

=== Download Drupal

To download Drupal, open your command line terminal and execute the commands listed below.


*For Mac & Linux*, execute the following commands:

....
mkdir drupal
cd drupal
curl -sSL https://www.drupal.org/download-latest/tar.gz | tar -xz --strip-components=1
....

*For Windows*, execute the following commands. Replace _x.y.z_ in the commands
below with the downloaded version of Drupal.

....
curl -sSL https://www.drupal.org/download-latest/zip --output drupal.zip
unzip drupal.zip
cd drupal-x.y.z
....

These commands will download a tarball of the link:/8/download[latest recommended version of Drupal], decompress it, and change into the resultant directory.

=== Install Drupal & Login

Next, execute the following command(s):

....
php core/scripts/drupal quick-start demo_umami
....

This will start a link:http://php.net/manual/en/features.commandline.webserver.php[built-in PHP webserver], install Drupal to a link:http://php.net/manual/en/sqlite3.installation.php[SQLite database], and open your web browser to visit the new Drupal site.

If you'd like to learn more about Drupal's built-in `quick-start` command, execute:

....
php core/scripts/drupal quick-start --help
....

=== What's Next

You're now logged in (as an administrator) to a Drupal demo application. This site is not intended for use as the starting point for a new custom site and is not suitable for a production environment.

When you're ready to get started with building your own Drupal application, visit the <<guide_getting_started>> guide.
